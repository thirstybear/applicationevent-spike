package uk.co.thirstybear.appeventeg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication application = new SpringApplication(Application.class);
    addInitHooks(application);
    application.run(args);
  }

  private static void addInitHooks(SpringApplication application) {
    application.addListeners((ApplicationListener<ApplicationEnvironmentPreparedEvent>) event -> {
      ConfigurableEnvironment environment = event.getEnvironment();

      printPropertyValueFor(environment, "my.property");

      changeValueIn(environment);

      printPropertyValueFor(environment, "my.property");
    });
  }

  private static void printPropertyValueFor(ConfigurableEnvironment environment, String key) {
    String property = environment.getProperty(key);
    System.out.println("+++ my.property = " + property + " +++");
  }

  private static void changeValueIn(ConfigurableEnvironment environment) {

    /*
        First, create a map of the key-newvalue pair(s)
     */
    Map<String, Object> map = new HashMap<>();
    map.put("my.property", "Mr Flibble");

    /*
        Then add it to the PropertySources
     */
    MutablePropertySources propertySources = environment.getPropertySources();
    propertySources.addFirst(new MapPropertySource("mymap", map));
  }
}
