package uk.co.thirstybear.appeventeg.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class HelloController {
  private final String myProperty;

  @Autowired
  public HelloController(@Value("${my.property}") String myProperty) {
    this.myProperty = myProperty;
  }

  @RequestMapping(value="/", method = GET, produces = APPLICATION_JSON_VALUE)
  public String showValues() {
    return "my.property is " + myProperty;
  }
}
