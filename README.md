# ApplicationEvent Listener Hooks Example

This repo is a try-out for intercepting Spring Boot ApplicationEvents to override configuration values before the app
has actually started (ie pre-Tomcat/whichever has launched).

## Explanation

The key is to define listeners for the type of event needed (in this case `ApplicationEnvironmentPreparedEvent`) and then 
redefine the required values.

Note the way that the value has to be updated inside the `ConfigurableEnvironment` - it's not a straightforward mapping!

More useful information is in the [Spring Documentation](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-spring-application.html#boot-features-application-events-and-listeners).

## This Application

This is a simple Spring Boot application with a user defined property in the `application.properties` file imaginatively 
titled `my.property`, and initially set to the equally imaginative value 
[`wibble`](http://blackadderquotes.com/funny-blackadder-meme-wibble).

The app is configured to intercept the `ApplicationEnvironmentPreparedEvent` and convert `my.property` to 
[`Mr Flibble`](https://reddwarf.fandom.com/wiki/Mr._Flibble).

The change can be confirmed by going to the default endpoint `/` (served by `HelloController`).

